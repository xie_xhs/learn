package com.lxy.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    public static String getDateF(){
        Date date = new Date();

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        return format.format(date); //2013-01-14
    }
}
