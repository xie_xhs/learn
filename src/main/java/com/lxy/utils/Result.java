package com.lxy.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "通用的返回值格式",value = "Result")
public class Result<T> {
    /**
     * 返回状态码
     */
    @ApiModelProperty(name = "code", value = "返回状态码", required = true)
    private Integer code;
    /**
     *返回信息描述
     */
    @ApiModelProperty(name = "message", value = "返回信息描述", required = true)
    private String message;
    /**
     *返回值
     */
    @ApiModelProperty(name = "data", value = "返回值", required = true)
    private  T data;
    private Result(){
        this.code = 200;
        this.message = "ok";
        this.data = null;
    }
    private Result(ResultCode resultCode,T data){
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.data = data;
    }

    /**
     * 暴露外面静态方法. 成功
     */
    public static <E> Result success(E data){
        return new Result<E>(ResultCode.SUCCESS,data);
    }

    /**
     * 失败
     */
    public static <E> Result fail(E data){
        return new Result<E>(ResultCode.FAIL,data);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


}
