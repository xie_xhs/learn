package com.lxy.service;

import com.lxy.entity.Admin;

/**
 * (Admin)表服务接口
**/
public interface AdminService {
    /**
     * 管理员登录
     * @param username 账号
     * @param pwd 密码
     * @return
     */
  Admin login(String username,String pwd);

}