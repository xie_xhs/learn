package com.lxy.service;

import com.lxy.entity.User;

import java.util.List;

/**
 * 用户业务服务类
 */
public interface UserService {
    /**
     * 登录
     * @param username 账号
     * @param password 密码
     */
    Object login(String username, String password);

    /**
     * 注册
     * @param user 表单
     * @return
     */
    Object register(User user);

    /**
     * 重复账号校验
     * @param username 账号
     * @return
     */
    Object checkUser(String username);

    /**
     * 获取用户信息
     * @param username 账号
     * @return
     */
    Object getUser(String username);

    /**
     * 设置用户信息
     * @param user 用户
     * @return
     */
    Object setUser(User user);

    /**
     * 获取全部用户数据
     * @return
     */
    List<User> getList();

    /**
     * 删除指定用户
     * @param username 账号
     * @return
     */
    boolean deleteUserById(String username);

    /**
     * 根据id更新用户信息
     * @param user
     * @return
     */
    boolean updateUserById(User user);
}
