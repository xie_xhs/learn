package com.lxy.service;

import com.lxy.entity.Study;

import java.util.List;

/**
 * 学习时长业务服务类
 *
 */
public interface StudyService {
    /**
     * 获取指定用户学习时长
     * @param userId 用户账号
     * @param date 日期
     * @return
     */
    Object getTimeByUser(String userId, String date);

    /**
     * 获取当前用户指定前几天学习时长
     * @param day 天数
     * @return
     */
    List<Study> getTime(String userId ,String day);

    /**
     * 添加一个用户的时长记录
     * @param username
     * @param time
     * @return
     */
    boolean addTime(String username,String time);

    /**
     * 获取全部用户数据
     * @return
     */
    List<Study> getList();
}
