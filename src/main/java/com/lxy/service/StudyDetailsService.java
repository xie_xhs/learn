package com.lxy.service;

import com.lxy.entity.StudyDetails;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StudyDetailsService {
    /**
     * 添加一个数据
     * @param sd
     * @return
     */
    boolean add(StudyDetails sd);

    /**
     * 查询用户某天的数据
     * @param username
     * @param date
     * @return
     */
    List<StudyDetails> list(String username,String date);
}