package com.lxy.service;

import com.lxy.entity.Energy;

import java.util.List;

public interface EnergyService {
    /**
     * 添加一个用户的积分信息
     * @param username 用户
     * @param value 积分值
     * @return
     */
    boolean addEnergy(String username,Integer value);

    /**
     * 获取指定用户的积分值
     * @param username 用户
     * @return
     */
    Energy getEnergy(String username);

    /**
     * 获取全部用户积分数据
     * @return
     */
    List<Energy> getList();
}
