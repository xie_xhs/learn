package com.lxy.service;

import com.lxy.entity.Contribution;

import java.util.List;

public interface ContributionService {
    /**
     * 添加一个用户的贡献信息
     * @param username 用户
     * @param value 积分值
     * @return
     */
    boolean addContribution(String username,Integer value);

    /**
     * 获取指定用户的贡献值
     * @param username 用户
     * @return
     */
    Contribution getContribution(String username);

    /**
     * 获取全部用户贡献值数据
     * @return
     */
    List<Contribution> getList();
}
