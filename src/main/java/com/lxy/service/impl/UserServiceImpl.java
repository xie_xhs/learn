package com.lxy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxy.dao.StudyDao;
import com.lxy.dao.UserDao;
import com.lxy.entity.User;
import com.lxy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    public UserDao userDao;
    @Resource
    public StudyDao studyDao;

    @Override
    public Object login(String username, String password) {
        //条件构造
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        wrapper.eq("password", password);
        //返回查询到的数据
        return userDao.selectOne(wrapper);
    }

    @Override
    public Object register(User user) {
        try {
            //插入一条数据
            int insert = userDao.insert(user);

            if (insert != 0) {
                //结果不为0，说明插入成功
                return true;
            }
        } catch (Exception e) {
            //捕获重复注册，主键唯一异常
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Object checkUser(String username) {
        //条件构造
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        List<User> users = userDao.selectList(wrapper);
        //存在一个或多个账号
        return users.size() == 0;
    }

    @Override
    public Object getUser(String username) {
        //条件构造
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        return userDao.selectOne(wrapper);
    }

    @Override
    public Object setUser(User user) {
        int i = userDao.updateById(user);
        return i != 0;
    }

    @Override
    public List<User> getList() {
        return userDao.selectList(null);
    }

    @Override
    public boolean deleteUserById(String username) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        //在删除用户的同是也应该删除积分、贡献和学习时长
        //删除积分
        //删除贡献
        //删除学习时长记录
        int i = userDao.delete(wrapper);
        //true 成功 ，false 失败
        return i != 0;
    }

    @Override
    public boolean updateUserById(User user) {
        int i = userDao.updateById(user);
        //true 成功 ，false 失败
        return i != 0;
    }
}
