package com.lxy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxy.dao.EnergyDao;
import com.lxy.entity.Energy;
import com.lxy.service.EnergyService;
import com.lxy.utils.UUIDutil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnergyServiceImpl implements EnergyService {
    @Autowired
    private EnergyDao energyDao;

    @Override
    public boolean addEnergy(String username, Integer value) {
        //先判断是否存在，不存在就新真增，存在就累加
        QueryWrapper<Energy> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        Energy res = energyDao.selectOne(wrapper);
        if (res != null) {
            //已存在，累加值
            Energy energy = new Energy();
            energy.setId(res.getId());
            energy.setUsername(res.getUsername());
            energy.setValue(res.getValue() + value);
            int i = energyDao.updateById(energy);
            return i != 0;
        }
        //不存在就新增
        Energy energy = new Energy();
        energy.setId(UUIDutil.getUUID());
        energy.setUsername(username);
        energy.setValue(value);
        int i = energyDao.insert(energy);
        return i != 0;
    }

    @Override
    public Energy getEnergy(String username) {
        QueryWrapper<Energy> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        return energyDao.selectOne(wrapper);
    }

    @Override
    public List<Energy> getList() {

        return energyDao.selectList(null);
    }
}
