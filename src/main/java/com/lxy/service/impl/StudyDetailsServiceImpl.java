package com.lxy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxy.dao.StudyDao;
import com.lxy.dao.StudyDetailsDao;
import com.lxy.entity.Study;
import com.lxy.entity.StudyDetails;
import com.lxy.service.StudyDetailsService;
import com.lxy.utils.UUIDutil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class StudyDetailsServiceImpl implements StudyDetailsService {
    @Resource
    private StudyDetailsDao studyDetailsDao;

    @Resource
    private StudyServiceImpl studyService;

    @Override
    public boolean add(StudyDetails sd) {
        //先查询数据库是否已存在该用户当天同类型的记录,存在累加时长，不存在新增
        QueryWrapper<StudyDetails> wrapper = new QueryWrapper<>();
        wrapper.eq("username", sd.getUsername());
        wrapper.eq("type", sd.getType());
        wrapper.eq("date", sd.getDate());
        StudyDetails one = studyDetailsDao.selectOne(wrapper);
        if (one != null) {
            //存在累加时长
            StudyDetails details = new StudyDetails();
            details.setId(one.getId());
            //转成成int类型累加运算，再转回string更新
            String str = String.valueOf(Integer.parseInt(one.getTime()) + Integer.parseInt(sd.getTime()));
            details.setTime(str);
            int i = studyDetailsDao.updateById(details);
            //累加总时长
            studyService.addTime(one.getUsername(),one.getTime());

            return i != 0;
        }
        //不存在新增
        sd.setId(UUIDutil.getUUID());
        int i = studyDetailsDao.insert(sd);
        //累加总时长
        studyService.addTime(sd.getUsername(),sd.getTime());
        return i != 0;
    }

    @Override
    public List<StudyDetails> list(String username, String date) {
        QueryWrapper<StudyDetails> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        wrapper.eq("date", date);
        return studyDetailsDao.selectList(wrapper);
    }
}