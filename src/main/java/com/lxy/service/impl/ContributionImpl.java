package com.lxy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxy.dao.ContributionDao;
import com.lxy.entity.Contribution;
import com.lxy.entity.Energy;
import com.lxy.service.ContributionService;
import com.lxy.utils.UUIDutil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContributionImpl implements ContributionService {
    @Autowired
    private ContributionDao contributionDao;

    @Override
    public boolean addContribution(String username, Integer value) {
        //先判断是否存在，不存在就新真增，存在就累加
        QueryWrapper<Contribution> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        Contribution res = contributionDao.selectOne(wrapper);
        if (res != null) {
            //已存在，累加值
            Contribution contribution = new Contribution();
            contribution.setId(res.getId());
            contribution.setUsername(res.getUsername());
            contribution.setValue(res.getValue() + value);
            int i = contributionDao.updateById(contribution);
            return i != 0;
        }
        //不存在就新增
        Contribution contribution = new Contribution();
        contribution.setId(UUIDutil.getUUID());
        contribution.setUsername(username);
        contribution.setValue(value);
        int i = contributionDao.insert(contribution);
        return i != 0;
    }

    @Override
    public Contribution getContribution(String username) {
        QueryWrapper<Contribution> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        return contributionDao.selectOne(wrapper);
    }

    @Override
    public List<Contribution> getList() {
        return contributionDao.selectList(null);
    }
}
