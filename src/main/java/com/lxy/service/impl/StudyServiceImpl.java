package com.lxy.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxy.dao.StudyDao;
import com.lxy.entity.Study;
import com.lxy.service.StudyService;
import com.lxy.utils.DateUtils;
import com.lxy.utils.UUIDutil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StudyServiceImpl implements StudyService {
    @Autowired
    private StudyDao studyDao;

    @Override
    public Object getTimeByUser(String userId, String date) {
        QueryWrapper<Study> wrapper = new QueryWrapper<>();
        wrapper.eq("username", userId);
        wrapper.eq("date", date);

        return studyDao.selectOne(wrapper);
    }

    @Override
    public List<Study> getTime(String userId, String day) {
        return studyDao.getTime(userId, day);
    }

    @Override
    public boolean addTime(String username, String time) {
        //先判断要插入的这个用户当天是否已经存在记录，如果不存在就新增，存在就累加
        QueryWrapper<Study> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        wrapper.eq("date", DateUtils.getDateF());
        Study result = studyDao.selectOne(wrapper);
        if (result != null) {
            //存在
            Study study = new Study();
            study.setId(result.getId());
            int t1 = Integer.parseInt(result.getTime());
//            System.out.println(t1);
            int t2 = Integer.parseInt(time);
//            System.out.println(t2);
            String t3 = String.valueOf(t1 + t2);
//            System.out.println(t3);
            study.setTime(t3);
            study.setUsername(result.getUsername());
            study.setDate(DateUtils.getDateF());
            int i = studyDao.updateById(study);
            return i != 0;
        }
        //不存在，新增
        Study study = new Study();
        study.setUsername(username);
        study.setTime(time);
        study.setId(UUIDutil.getUUID());
        study.setDate(DateUtils.getDateF());
        int i = studyDao.insert(study);
        return i != 0;
    }

    @Override
    public List<Study> getList() {
        QueryWrapper<Study> wrapper = new QueryWrapper<>();
        //根据日期排序
        wrapper.orderBy(true,true,"date");
        return studyDao.selectList(wrapper);
    }


}
