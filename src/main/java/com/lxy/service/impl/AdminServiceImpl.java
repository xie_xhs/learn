package com.lxy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxy.dao.AdminDao;
import com.lxy.entity.Admin;
import com.lxy.service.AdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * (Admin)表服务实现类

 */
@Service("adminService")
public class AdminServiceImpl implements AdminService {
    @Resource
    private AdminDao adminDao;


    @Override
    public Admin login(String username, String pwd) {
        QueryWrapper<Admin> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        wrapper.eq("password",pwd);
        return adminDao.selectOne(wrapper);
    }
}