package com.lxy.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * (Study)实体类
 *
 * @author makejava
 * @since 2021-02-20 14:15:13
 */
@ApiModel(description = "学习时长",value = "Study")
public class Study implements Serializable {
    private static final long serialVersionUID = 663669077840255754L;
    /**
    * 主键id
    */
    @TableId("id")
    @ApiModelProperty(name = "id", value = "主键id", required = true)
    private String id;
    /**
    * 学习的时长
    */
    @ApiModelProperty(name = "time", value = "学习的时长", required = true)
    private String time;
    /**
    * 学习的日期
    */
    @ApiModelProperty(name = "date", value = "学习的日期（2021-02-07）", required = true)
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private String date;
    /**
    * 该时长的用户id
    */
    @ApiModelProperty(name = "username", value = "该时长的用户id", required = true)
    private String username;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}