package com.lxy.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


@ApiModel(description = "用户",value = "User")
public class User implements Serializable {
    private static final long serialVersionUID = -77224306177076968L;
    /**
    * 账号（学号）
    */
    @TableId("username")
    @ApiModelProperty(name = "username", value = "账号（学号）", required = true)
    private String username;
    /**
    * 密码
    */
    @ApiModelProperty(name = "password", value = "密码", required = true)
    private String password;
    /**
    * 姓名
    */
    @ApiModelProperty(name = "name", value = "姓名", required = true)
    private String name;
    /**
    * 性别
    */
    @ApiModelProperty(name = "sex", value = "性别", required = true)
    private String sex;
    /**
    * 班级
    */
    @ApiModelProperty(name = "clas", value = "班级", required = false)
    private String clas;
    /**
    * 手机号
    */
    @ApiModelProperty(name = "phone", value = "手机号", required = false)
    private String phone;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getClas() {
        return clas;
    }

    public void setClas(String clas) {
        this.clas = clas;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}