package com.lxy.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "积分",value = "Energy")
public class Energy implements Serializable {
    private static final long serialVersionUID = 611430156936166759L;
    /**
    * 主键id
    */
    @TableId("id")
    @ApiModelProperty(name = "id", value = "主键id", required = true)
    private String id;
    /**
    * 关联的账号
    */
    @ApiModelProperty(name = "username", value = "关联的账号", required = true)
    private String username;
    /**
    * 能量值
    */
    @ApiModelProperty(name = "value", value = "能量值", required = true)
    private Integer value;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

}