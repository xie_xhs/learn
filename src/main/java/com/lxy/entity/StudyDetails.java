package com.lxy.entity;

import java.io.Serializable;


public class StudyDetails implements Serializable {
    private static final long serialVersionUID = -11571530148331063L;
    /**
    * 主键id
    */
    private String id;
    /**
    * 账号
    */
    private String username;
    /**
    * 日期
    */
    private Object date;
    /**
    * 学习类型
    */
    private String type;
    /**
    * 学习时长
    */
    private String time;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Object getDate() {
        return date;
    }

    public void setDate(Object date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}