package com.lxy.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel(description = "管理员",value = "Admin")
public class Admin implements Serializable {
    private static final long serialVersionUID = 411379284809559052L;
    /**
    * id
    */
    @TableId("id")
    private String id;
    /**
    * 账号
    */
    private String username;
    /**
    * 密码
    */
    private String password;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}