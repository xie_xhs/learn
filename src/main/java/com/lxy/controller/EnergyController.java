package com.lxy.controller;

import com.lxy.entity.Energy;
import com.lxy.service.impl.EnergyServiceImpl;
import com.lxy.utils.Result;
import com.lxy.utils.ResultCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "用户Energy操作接口")
public class EnergyController {
    @Autowired
    private EnergyServiceImpl energyService;

    /**
     * 获取用户积分接口
     * @param username 账号
     * @return
     */
    @ApiOperation("获取用户积分接口")
    @GetMapping("energy/{username}")
    public Result getEnergy(@PathVariable("username") String username) {
        if (username.equals("")) {
            return Result.fail(ResultCode.PARAM_IS_BLANK);
        }

        Energy energy = energyService.getEnergy(username);
        if (energy != null) {
            return Result.success(energy);
        }

        return Result.fail(ResultCode.FAIL);
    }

    /**
     * 新增一个用户积分接口
     * @param username 账号
     * @param value 积分
     * @return
     */
    @ApiOperation("新增一个用户积分接口")
    @PostMapping("energy")
    public Result setEnergy(String username, String value) {
        if (username.equals("") & value == null) {
            return Result.fail(ResultCode.PARAM_IS_BLANK);
        }
        if (energyService.addEnergy(username, Integer.parseInt(value))) {
            return Result.success(ResultCode.SUCCESS);
        }
        return Result.fail(ResultCode.FAIL);
    }

    /**
     * 获取全部用户积分信息
     * @return
     */
    @ApiOperation("获取全部用户积分信息")
    @GetMapping("energy")
    public Result getList(){
        return Result.success(energyService.getList());
    }
}
