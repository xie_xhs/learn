package com.lxy.controller;

import com.lxy.entity.StudyDetails;
import com.lxy.service.StudyDetailsService;
import com.lxy.utils.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
public class StudyDetailsController {
    /**
     * 服务对象
     */
    @Resource
    private StudyDetailsService studyDetailsService;

    @PostMapping("/sd")
    public Result add(@RequestBody StudyDetails sd){
        boolean flag = studyDetailsService.add(sd);
        if (flag){
            return Result.success("添加成功");
        }
        return Result.fail("添加失败");
    }
    @GetMapping("/sd/{id}/{day}")
    public Result list(@PathVariable("id") String username,@PathVariable("day")String date){
        List<StudyDetails> list = studyDetailsService.list(username, date);
        if (list.size() !=0){
            return Result.success(list);
        }
        return Result.fail("查询失败");
    }
}