package com.lxy.controller;

import com.lxy.entity.Admin;
import com.lxy.service.AdminService;
import com.lxy.utils.Result;
import com.lxy.utils.ResultCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
/**
 * @author xiehusheng
 * @description
 * @date 2021/7/21 20:52
 */
@Api(tags = "管理员admin操作接口")
@RestController
@RequestMapping("admin")
public class AdminController {

    @Resource
    private AdminService adminService;


    /**
     * @author xiehusheng
     * @description 管理员登录接口
     * @date 2021/7/21 20:36
    */
    @ApiOperation(value = "作者：谢虎生; 管理员登录接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username",value = "账号"),
            @ApiImplicitParam(name = "pwd",value = "密码")
    })
    @PostMapping("/login")
    public Result login(String username, String pwd){
        if (username ==null & pwd==null) {
            Result.fail(ResultCode.PARAM_IS_BLANK);
        }
        Admin login = adminService.login(username, pwd);
        if (login!=null){

            return Result.success(login);
        }
        return Result.fail(ResultCode.USER_LOGIN_ERROR);
    }
}