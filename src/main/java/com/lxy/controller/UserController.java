package com.lxy.controller;

import com.lxy.entity.User;
import com.lxy.service.impl.UserServiceImpl;
import com.lxy.utils.Result;
import com.lxy.utils.ResultCode;
import com.lxy.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Api(tags = "用户User操作接口")
@RestController
public class UserController {
    @Resource
    public UserServiceImpl userService;

    /**
     * 登录接口
     *
     * @param username
     * @param password
     * @return
     */
    @ApiOperation("登录接口")
    @PostMapping("/login")
    public Result login(@RequestParam("username") String username, @RequestParam("password") String password) {
        //参数非空判断
        if (username.equals("") || password.equals("")) {
            return Result.fail(ResultCode.PARAM_IS_BLANK);
        }
        //登录业务
        User result = (User) userService.login(username, password);
        if (null != result) {
            //登录成功后，把用户信息返回
            Map<String, Object> map = new HashMap<>();
            map.put("username", result.getUsername());
            map.put("name", result.getName());
            map.put("class", result.getClas());
            map.put("phone", result.getPhone());
            map.put("sex", result.getSex());
            return Result.success(map);
        }
        //登录失败，账号或密码错误
        return Result.fail(ResultCode.USER_LOGIN_ERROR);
    }

    /**
     * 注册接口
     *
     * @param user
     * @return
     */
    @ApiOperation("注册接口")
    @PostMapping("/register")
    public Result register(@RequestBody User user) {
        //表单非空判断

        //这里的user对象有bug,传过来的对象不会空，我需要判断对象的属性值是否为空。。。暂时没想到办法来处理
//        if (user == null) {
//            return Result.fail(ResultCode.PARAM_IS_BLANK);
//        }
        //写了个工具类来处理实体类属性判空，已解决bug
        if (!StringUtils.isNotEmptyBean(user)) {
            return Result.fail(ResultCode.PARAM_IS_BLANK);
        }
        //注册业务
        Boolean result = (Boolean) userService.register(user);
        if (result) {
            return Result.success(ResultCode.SUCCESS);
        }
        return Result.fail(ResultCode.USER_HAS_EXISTED);
    }

    /**
     * 账号重复校验接口
     *
     * @param username
     * @return
     */
    @ApiOperation("账号重复校验接口")
    @GetMapping("/checkUser/{userId}")
    public Result checkUser(@PathVariable("userId") String username) {
        //参数非空判断
        if (username == null) {
            return Result.fail(ResultCode.PARAM_IS_BLANK);
        }
        Boolean result = (Boolean) userService.checkUser(username);
        if (result) {
            //校验通过，账号不存在
            return Result.success(ResultCode.USER_NOT_EXISTS);
        }
        //校验不通过，账号已存在
        return Result.success(ResultCode.USER_HAS_EXISTED);
    }

    /**
     * 获取用户信息接口
     *
     * @param username 账号
     * @return
     */
    @ApiOperation("获取用户信息接口")
    @GetMapping("/user/{userId}")
    public Result getUser(@PathVariable("userId") String username) {
        //账号参数为空
        if (username == null) {
            return Result.fail(ResultCode.PARAM_IS_BLANK);
        }
        User user = (User) userService.getUser(username);
        if (null != user) {
            //返回用户信息
            return Result.success(user);
        }
        //用户不存在
        return Result.fail(ResultCode.USER_NOT_EXISTS);
    }

    /**
     * 插入用户信息接口
     *
     * @param user 表单
     * @return
     */
    @ApiOperation("插入用户信息接口")
    @PostMapping("/user")
    public Result setUser(@RequestBody User user) {

        //表单需要修改的用户参数为空
        if (user == null || user.getUsername().equals("")) {
            return Result.fail(ResultCode.PARAM_IS_BLANK);
        }
        //更新的任意一个数据为空
        if (user.getPassword().equals("") && user.getName().equals("") && user.getClas().equals("") && user.getPhone().equals("") && user.getSex().equals("")) {
            return Result.fail(ResultCode.PARAM_NOT_COMPLETE);
        }
        Boolean result = (Boolean) userService.setUser(user);
        if (result) {
            //返回跟新结果
            return Result.success(ResultCode.SUCCESS);
        }
        return Result.fail(ResultCode.FAIL);
    }

    /**
     * 获取全部用户数据
     *
     * @return
     */
    @ApiOperation("获取全部用户数据")
    @GetMapping("/user")
    public Result listUser() {
        return Result.success(userService.getList());
    }

    /**
     * 更新指定用户的信息
     * @param user
     * @return
     */
    @ApiOperation("更新指定用户的信息")
    @PutMapping("/user")
    public Result updateUserById(@RequestBody User user) {
        Boolean flag = StringUtils.isNotEmptyBean(user);
        if (!flag) {
            return Result.fail(ResultCode.PARAM_NOT_COMPLETE);
        }
        boolean b = userService.updateUserById(user);
        if (b) {
            return Result.success("用户信息更新成功");
        }

        return Result.fail("更新出现错误");
    }

    /**
     * 删除指定用户
     * @param username 账号
     * @return
     */
    @DeleteMapping("/user/{id}")
    public Result deleteUserById(@PathVariable("id") String username){
        if (username == null) {
            return Result.fail(ResultCode.PARAM_NOT_COMPLETE);
        }
        boolean flag = userService.deleteUserById(username);
        if (flag) {
            return Result.success("删除成功");
        }
        return Result.fail("删除失败");
    }
}
