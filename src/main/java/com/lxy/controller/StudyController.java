package com.lxy.controller;

import com.lxy.entity.Study;
import com.lxy.service.impl.StudyServiceImpl;
import com.lxy.utils.Result;
import com.lxy.utils.ResultCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Api(tags = "用户Study操作接口")
@RestController
public class StudyController {

    @Autowired
    private StudyServiceImpl studyService;

    /**
     * 获取指定用户某天的学习时长
     * @param userId 指定用户
     * @param date 指定日期（2021-02-07）
     * @return
     */
    @ApiOperation("获取指定用户某天的学习时长")
    @GetMapping("time/{username}/{date}")
    public Result getStudyByUser(@PathVariable("username") String userId, @PathVariable("date") @ApiParam(name = "date" ,value = "日期(2021-02-07)")String date){
        if ( userId.equals("") & date.equals("")){
            return Result.fail(ResultCode.PARAM_NOT_COMPLETE);
        }

        Study timeByUser = (Study) studyService.getTimeByUser(userId, date);
        if (timeByUser !=null) {
            return Result.success(timeByUser);
        }
        return Result.fail(ResultCode.FAIL);
    }

    /**
     * 获取指定用户某几天内的学习时长
     *
     * @param userId 指定用户
     * @param day    天数
     * @return
     */
    @ApiOperation("获取指定用户某几天内的学习时长")
    @GetMapping("times/{username}/{day}")
    public Result getTime(@PathVariable("username") String userId, @PathVariable("day") @ApiParam(name = "day", value = "天数(6或者30)") String day) {
        if (userId.equals("") & day.equals("")) {
            return Result.fail(ResultCode.PARAM_NOT_COMPLETE);
        }
        List<Study> list = studyService.getTime(userId, day);
        if (list != null) {
            return Result.success(list);

        }
        return Result.fail(ResultCode.FAIL);
    }

    /**
     * 添加一个指定用户的当天学习时长
     * @param username 指定用户的id
     * @param time 时长（秒）
     * @return
     */
    @ApiOperation("添加一个指定用户的当天学习时长")
    @PostMapping("time")
    public Result addTime(@RequestParam String username, @ApiParam(name = "time", value = "时长(秒)")@RequestParam String time){
        if (username.equals("") & time.equals("")) {
            return  Result.fail(ResultCode.PARAM_IS_BLANK);
        }
        boolean b = studyService.addTime(username, time);
        if (b){
            return Result.success(ResultCode.SUCCESS);
        }
        return Result.fail(ResultCode.FAIL);
    }

    /**
     * 获取全部的时长数据
     * @return
     */
    @ApiOperation("获取全部的时长数据")
    @GetMapping("/time")
    public Result list() {

        return Result.success(studyService.getList());
    }
}
