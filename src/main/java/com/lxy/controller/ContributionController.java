package com.lxy.controller;

import com.lxy.entity.Contribution;
import com.lxy.service.impl.ContributionImpl;
import com.lxy.utils.Result;
import com.lxy.utils.ResultCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "用户Contribution操作接口")
public class ContributionController {
    @Autowired
    private ContributionImpl contributionService;

    /**
     * 获取用户贡献值接口
     * @param username 账号
     * @return
     */
    @ApiOperation("获取用户贡献值接口")
    @GetMapping("Contribution/{username}")
    public Result getContribution(@PathVariable("username") String username) {
        if (username.equals("")) {
            return Result.fail(ResultCode.PARAM_IS_BLANK);
        }
        Contribution contribution = contributionService.getContribution(username);
        if (contribution != null) {
            return Result.success(contribution);
        }
        return Result.fail(ResultCode.FAIL);
    }


    /**
     * 新增一个用户贡献值接口
     * @param username 账号
     * @param value 贡献值
     * @return
     */
    @ApiOperation("新增一个用户贡献值接口")
    @PostMapping("Contribution")
    public Result setContribution(String username, String value) {
        if (username == null & value == null) {
            return Result.fail(ResultCode.PARAM_IS_BLANK);
        }
        boolean b = contributionService.addContribution(username, Integer.parseInt(value));
        if (b) {
            return Result.success(ResultCode.SUCCESS);
        }
        return Result.fail(ResultCode.FAIL);
    }

    /**
     * 获取全部用户贡献值
     * @return
     */
    @ApiOperation("获取全部用户贡献值")
    @GetMapping("Contribution")
    public Result getList(){
        return Result.success(contributionService.getList());
    }
}
