package com.lxy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxy.entity.User;
import org.apache.ibatis.annotations.Mapper;

import javax.annotation.Resource;

@Mapper
public interface UserDao extends BaseMapper<User> {
    /**
     * 。。。。。。。。可以自定义
     */
}