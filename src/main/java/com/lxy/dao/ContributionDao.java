package com.lxy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxy.entity.Contribution;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ContributionDao extends BaseMapper<Contribution> {

/**
 * 自定义其他方法
 */

}