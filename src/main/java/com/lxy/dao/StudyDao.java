package com.lxy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxy.entity.Study;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface StudyDao extends BaseMapper<Study> {

    /**
     * 自定义其他方法
     */


    /**
     * 获取当前用户指定前几天学习时长
     * @param day 天数
     * @return
     */
    @Select("SELECT * FROM study WHERE DATE_SUB(CURDATE(), INTERVAL #{day} DAY) <= DATE(`date`) AND username=#{userId} GROUP BY `date`")
    List<Study> getTime(String userId, String day);


}