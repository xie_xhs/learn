package com.lxy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxy.entity.Admin;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author xiehusheng
 * @description
 * @date 2021/7/21 20:56
 */
@Mapper
public interface AdminDao extends BaseMapper<Admin> {


}